# Computer Architecture

Those attributes that have direct impact on logical execution of a program.

Closely related to **Instruction Set Architecture (ISA)**

- Instruction formats
- OpCodes
- Registers
- Instruction and Data Memory
- The effect of executed instruction on memory and registers
- Algorithms for controlling instruction execution

# Computer Organization

Operational units and their interconnections that realize the architectural specifications.

- Instruction set
- Number of bits used to represent various data
- I/O Mechanism
- Techniques for addressing memory
- Control Signals
- Interfaces between the computer and peripherals
- The memory technologies used

# Example

- It is architectural design issue wether a computer will have a multiply instruction.
- It is organizational design issue that instruction will be implemented by a special multiply unit or by a mechanism that makes use of add unit of the system.
- ### The organization design may be based on:
  - Anticipated frequency of the use of multiply
  - Relative speed of the two approaches
  - Cost and Physical size of special multiply unit

# Function

There are 4 basic functions that a computer can perform.

- *Data Processing*
- *Data Storage*
  - Short Term storage
  - Long Term storage
- *Data Movement*
  - ***Input - Output***: The data received or delivered between two connected devices.
  - ***Perpherals***: Directly connected devices 
  - ***Data Communications***: The data movement over a large distances between remote devices.
- *Control*: Manages the computer's resources and orchestrates the performance.

# Structure
## Single Core Structure:
There are 4 main structural components

![Computer Top Level Structure](images/Compute%20Top%20Level%20Structure.png)

### Main Components:
- *Central Processing Unit*: Controls the operation of the computer and performs its data processing functions. Also known as **Processor**.
- *Main Memory*: Stores Data
- *I/O*: Moves data between computer and its external environment
- *System Bus*: A mechanism that provides for communication among CPU, main memory, and I/O.

### CPU:
- *Control Unit*: Controls the operation of the CPU and hence the computer.
- *Arithmetic and Logic Unit (ALU)*: Perform's computers data processing functions.
- *Registers*: Provides storage internal to CPU
- *CPU Interconnection*: Provides a mechanism for communication among control unit, ALU and Registers.

## Multi Core Structure:
More than one processors reside on single chip is referred to as multicore structure.

Each processing unit (Control Unit + ALU + Registers + Cache) is called as a core.


![Computer Simplified Multicore View](images/Simplified%20View%20of%20Multicore%20Computer.png)

# Memory
- Registers
- Cache Memory
- Main Memory
- Auxilary Memory
